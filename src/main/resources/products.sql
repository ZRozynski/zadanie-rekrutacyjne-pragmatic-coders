INSERT INTO Products (id, name, price) values (1, 'Apple', 1.0);
INSERT INTO Products (id, name, price) values (2, 'Peach', 2.0);
INSERT INTO Products (id, name, price) values (3, 'Banana', 3.0);
INSERT INTO Products (id, name, price) values (4, 'Carrot', 4.0);
INSERT INTO Products (id, name, price) values (5, 'Cucumber', 5.0);
INSERT INTO Products (id, name, price) values (6, 'Hummus', 6.0);
INSERT INTO Products (id, name, price) values (7, 'Tomato', 7.0);