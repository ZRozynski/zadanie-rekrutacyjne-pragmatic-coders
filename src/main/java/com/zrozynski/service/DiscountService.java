package com.zrozynski.service;

import com.zrozynski.domain.Basket;
import com.zrozynski.domain.Checkout;
import com.zrozynski.domain.Product;
import com.zrozynski.domain.discount.Discount;
import com.zrozynski.repository.DiscountRepository;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class DiscountService {

    @Setter
    @Autowired
    DiscountRepository repository;

    public Checkout applyDiscounts(Basket basket) {

        Map<Product, Integer> products = basket.getProducts();
        double totalPrice = getBasketOriginalAmount(basket);
        List<Discount> discounts = getAllDiscounts();

        double discountAmount = discounts
                .stream()
                .mapToDouble(discount -> discount.apply(products))
                .sum();

        return checkoutOf(basket, totalPrice, totalPrice - discountAmount);
    }

    private double getBasketOriginalAmount(Basket basket) {
        Map<Product, Integer> products = basket.getProducts();
        return products
                .entrySet()
                .stream()
                .mapToDouble(entry -> entry.getKey().getPrice() * entry.getValue())
                .sum();
    }

    private Checkout checkoutOf(Basket basket, double originalPrice, double priceAfterDiscount) {
        return new Checkout(basket.getProducts(), originalPrice, priceAfterDiscount);
    }

    public List<Discount> getAllDiscounts() {
        return repository.findAll();
    }
}
