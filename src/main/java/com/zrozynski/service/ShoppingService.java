package com.zrozynski.service;

import com.zrozynski.domain.Basket;
import com.zrozynski.domain.Checkout;
import com.zrozynski.domain.Product;
import com.zrozynski.domain.discount.Discount;
import com.zrozynski.repository.ProductRepository;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Setter
public class ShoppingService {

    @Getter
    @Autowired
    private Basket basket;
    @Autowired
    private DiscountService discountService;
    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Optional<Product> addProductWithQuantity(String productName, Integer quantity) {
        Product product = productRepository.findByName(productName);
        return basket.addProductWithQuantity(product, quantity);
    }
    public List<Discount> getAvailableDiscounts() {
        return discountService.getAllDiscounts();
    }

    public Checkout checkout() {
        Checkout checkout = discountService.applyDiscounts(basket);
        basket = new Basket();
        return checkout;
    }
}
