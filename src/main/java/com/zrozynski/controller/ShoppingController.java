package com.zrozynski.controller;

import com.zrozynski.domain.Basket;
import com.zrozynski.domain.Checkout;
import com.zrozynski.domain.Product;
import com.zrozynski.domain.discount.Discount;
import com.zrozynski.service.ShoppingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("shop")
public class ShoppingController {
    @Autowired
    ShoppingService service;

    @GetMapping("/all")
    public List<Product> getAll() {
        return service.getAllProducts();
    }

    @GetMapping("basket")
    public Basket getBasketContent(){
        return service.getBasket();
    }

    @PostMapping("add")
    public Optional<Product> addProductWithQuantity(@RequestParam String productName, @RequestParam Integer quantity) {
        return service.addProductWithQuantity(productName, quantity);
    }

    @GetMapping("checkout")
    public Checkout getCheckout(){
        return service.checkout();
    }

    @GetMapping("discounts")
    public List<Discount> getAvailableDiscounts(){
        return service.getAvailableDiscounts();
    }
}
