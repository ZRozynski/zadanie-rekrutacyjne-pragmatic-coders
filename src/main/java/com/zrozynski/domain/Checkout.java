package com.zrozynski.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@Data
@AllArgsConstructor
public class Checkout {
    private Map<Product, Integer> products;
    private double priceBefore;
    private double priceAfterDiscount;
}
