package com.zrozynski.domain;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Data
@Component
public class Basket {
    Map<Product, Integer> products;

    public Optional<Product> addProductWithQuantity(Product product, Integer quantity) {

        if (product == null) {
            return Optional.empty();
        }
        if (products.containsKey(product)) {
            Integer oldProductQuantity = products.get(product);
            quantity += oldProductQuantity;
        }
        products.put(product, quantity);
        return Optional.of(product);
    }

    {
        products = new HashMap<>();
    }
}
