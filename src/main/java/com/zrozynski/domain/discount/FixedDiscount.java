package com.zrozynski.domain.discount;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("Fixed")
public class FixedDiscount extends Discount {

    private double priceAfterDiscount;

    @Override
    protected double getDiscountAmount() {
        return getProductsOriginalPrice() - priceAfterDiscount;
    }
}
