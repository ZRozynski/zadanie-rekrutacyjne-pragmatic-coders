package com.zrozynski.domain.discount;

import com.zrozynski.domain.Product;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "Discounts")
@AllArgsConstructor
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "discount_type")
public abstract class Discount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Setter
    @OneToMany
    @JoinColumn(name = "discount_id")
    private List<DiscountProductWithQuantity> requiredProducts;

    protected abstract double getDiscountAmount();

    protected double getProductsOriginalPrice() {
        return requiredProducts
                .stream()
                .mapToDouble(prod -> prod.getProduct().getPrice() * prod.getQuantity())
                .sum();
    }

    public double apply(Map<Product, Integer> basket){

        List<DiscountProductWithQuantity> filteredRequiredProducts = requiredProducts
                .stream()
                .filter(rProd -> basket.containsKey(rProd.getProduct()) && basket.get(rProd.getProduct()) == rProd.getQuantity())
                .collect(Collectors.toList());

        return filteredRequiredProducts.size() == requiredProducts.size() ? getDiscountAmount() : 0;
    }
}
