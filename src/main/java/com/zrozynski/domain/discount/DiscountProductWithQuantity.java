package com.zrozynski.domain.discount;

import com.zrozynski.domain.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class DiscountProductWithQuantity {
    @Id
    @GeneratedValue
    private Long id;
    @OneToOne
    private Product product;
    private Integer quantity;
}
