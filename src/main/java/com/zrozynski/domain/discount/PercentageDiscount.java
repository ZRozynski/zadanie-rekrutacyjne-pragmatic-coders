package com.zrozynski.domain.discount;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("Percentage")
public class PercentageDiscount extends Discount {

    private double percentageAmount;

    @Override
    protected double getDiscountAmount() {
        double originalPrice = getProductsOriginalPrice();
        return (originalPrice * percentageAmount) / 100;
    }
}
