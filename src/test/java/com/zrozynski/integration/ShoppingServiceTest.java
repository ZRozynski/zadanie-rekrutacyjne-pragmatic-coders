package com.zrozynski.integration;

import com.zrozynski.domain.Basket;
import com.zrozynski.domain.Checkout;
import com.zrozynski.domain.Product;
import com.zrozynski.domain.discount.Discount;
import com.zrozynski.repository.DiscountRepository;
import com.zrozynski.repository.ProductRepository;
import com.zrozynski.service.DiscountService;
import com.zrozynski.service.ShoppingService;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Category(IntegrationTest.class)
@RunWith(SpringRunner.class)
@DataJpaTest
public class ShoppingServiceTest {

    private static ShoppingService shoppingService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private DiscountRepository discountRepository;

    @Before
    public void initializeShoppingService() {
        shoppingService = new ShoppingService();

        shoppingService.setBasket(new Basket());

        DiscountService discountService = new DiscountService();
        discountService.setRepository(discountRepository);

        shoppingService.setProductRepository(productRepository);
        shoppingService.setDiscountService(discountService);
    }

    @Test
    public void shouldInjectRequiredServices() {
        assertNotNull(productRepository);
        assertNotNull(discountRepository);
    }

    @Test
    public void whenOneProductAddedToBasket_thenShouldReturnBasketSizeOfOneWithCorrectProductNameAndQuantity() {

        //when
        shoppingService.addProductWithQuantity("Apple", 3);
        //then
        assertEquals(1, shoppingService.getBasket().getProducts().size());
    }

    @Test
    public void whenTwoProductsAddedToBasket_thenShouldReturnBasketSizeOfTwoWithCorrectProductsNameAndQuantities() {

        //when
        shoppingService.addProductWithQuantity("Apple", 3);
        shoppingService.addProductWithQuantity("Banana", 2);

        //then
        assertEquals(2, shoppingService.getBasket().getProducts().size());
    }

    @Test
    public void whenProductOfSameNameAddedToBasket_thenShouldIncreaseCurrentProductQuantity() {

        //when
        shoppingService.addProductWithQuantity("Apple", 3);
        Optional<Product> apple = shoppingService.addProductWithQuantity("Apple", 1);

        //then
        assertEquals(1, shoppingService.getBasket().getProducts().size());
        assertEquals(Integer.valueOf(4), shoppingService.getBasket().getProducts().get(apple.get()));
    }

    @Test
    public void shouldReturnAllDiscounts() {

        //when
        List<Discount> discounts = shoppingService.getAvailableDiscounts();

        //then
        assertNotNull(discounts);
        assertEquals(4, discounts.size());
    }

    @Test
    public void shouldReturnCheckoutWithoutAnyDiscount() {

        //given
        shoppingService.addProductWithQuantity("Apple", 1);
        shoppingService.addProductWithQuantity("Banana", 1);
        shoppingService.addProductWithQuantity("Tomato", 1);

        //when
        Checkout checkout = shoppingService.checkout();

        //then
        assertNotNull(checkout);
        assertEquals(Double.doubleToLongBits(11), Double.doubleToLongBits(checkout.getPriceAfterDiscount()));
    }

    @Test
    public void shouldReturnCheckoutWithDiscount() {
        //given
        shoppingService.addProductWithQuantity("Cucumber", 3);

        //when
        Checkout checkout = shoppingService.checkout();

        //then
        assertNotNull(checkout);
        assertEquals(Double.doubleToLongBits(10), Double.doubleToLongBits(checkout.getPriceAfterDiscount()));
    }
}
