package com.zrozynski.service;

import com.zrozynski.domain.Basket;
import com.zrozynski.domain.Checkout;
import com.zrozynski.domain.Product;
import com.zrozynski.domain.discount.Discount;
import com.zrozynski.domain.discount.DiscountProductWithQuantity;
import com.zrozynski.domain.discount.FixedDiscount;
import com.zrozynski.domain.discount.PercentageDiscount;
import com.zrozynski.repository.DiscountRepository;
import com.zrozynski.repository.ProductRepository;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import static org.junit.Assert.assertEquals;

public class ShoppingServiceTest {

    private static ShoppingService shoppingService;

    @BeforeClass
    public static void initializeShoppingService() {
        shoppingService = new ShoppingService();
    }

    @Before
    public void setNewBasketForCurrentService() {
        shoppingService.setBasket(new Basket());
    }

    @Test
    public void whenOneProductAddedToBasket_thenShouldReturnBasketSizeOfOneWithCorrectProductNameAndQuantity() {

        //given
        ProductRepository mockedRepository = mock(ProductRepository.class);
        when(mockedRepository.findByName("Apple")).thenReturn(new Product(1L, "Apple", 1.0));
        shoppingService.setProductRepository(mockedRepository);

        //when
        Optional<Product> apple = shoppingService.addProductWithQuantity("Apple", 3);

        //then
        assertEquals(1, shoppingService.getBasket().getProducts().size());
        assertEquals("Apple", apple.get().getName());
        assertEquals(new Integer(3), shoppingService.getBasket().getProducts().get(apple.get()));
    }

    @Test
    public void whenTwoProductsAddedToBasket_thenShouldReturnBasketSizeOfTwoWithCorrectProductsNameAndQuantities() {

        //given
        ProductRepository mockedRepository = mock(ProductRepository.class);
        when(mockedRepository.findByName("Apple")).thenReturn(new Product(1L, "Apple", 1.0));
        when(mockedRepository.findByName("Peach")).thenReturn(new Product(2L, "Peach", 2.0));
        shoppingService.setProductRepository(mockedRepository);

        //when
        Optional<Product> apple = shoppingService.addProductWithQuantity("Apple", 3);
        Optional<Product> peach = shoppingService.addProductWithQuantity("Peach", 2);

        //then
        assertEquals(2, shoppingService.getBasket().getProducts().size());
        assertEquals("Apple", apple.get().getName());
        assertEquals("Peach", peach.get().getName());
        assertEquals(new Integer(3), shoppingService.getBasket().getProducts().get(apple.get()));
        assertEquals(new Integer(2), shoppingService.getBasket().getProducts().get(peach.get()));
    }

    @Test
    public void whenProductOfSameNameAddedToBasket_thenShouldIncreaseCurrentProductQuantity() {

        //given
        ProductRepository mockedRepository = mock(ProductRepository.class);
        when(mockedRepository.findByName("Apple")).thenReturn(new Product(1L, "Apple", 1.0));
        shoppingService.setProductRepository(mockedRepository);
        Product apple = mockedRepository.findByName("Apple");

        //when
        shoppingService.addProductWithQuantity("Apple", 1);
        shoppingService.addProductWithQuantity("Apple", 1);

        //then
        assertEquals(1, shoppingService.getBasket().getProducts().size());
        assertEquals(new Integer(2), shoppingService.getBasket().getProducts().get(apple));
    }

    @Test
    public void shouldReturnAllDiscounts() {
        //given
        shoppingService.setDiscountService(getMockedDiscountService());

        //when
        List<Discount> discounts = shoppingService.getAvailableDiscounts();

        //then
        assertEquals(2, discounts.size());
    }

    @Test
    public void shouldReturnCheckoutWithoutAnyDiscount() {

        //given
        ProductRepository mockedRepository = mock(ProductRepository.class);
        when(mockedRepository.findByName("Hummus")).thenReturn(new Product(1L, "Hummus", 10.0));
        when(mockedRepository.findByName("Carrot")).thenReturn(new Product(1L, "Carrot", 10.0));
        when(mockedRepository.findByName("Butter")).thenReturn(new Product(1L, "Butter", 10.0));
        shoppingService.setProductRepository(mockedRepository);
        shoppingService.setDiscountService(getMockedDiscountService());

        shoppingService.addProductWithQuantity("Hummus", 1);
        shoppingService.addProductWithQuantity("Carrot", 1);
        shoppingService.addProductWithQuantity("Butter", 1);

        //when
        Checkout checkout = shoppingService.checkout();

        //then
        assertEquals(0, shoppingService.getBasket().getProducts().size());
        assertEquals(3, checkout.getProducts().size());
        assertEquals(Double.doubleToLongBits(checkout.getPriceBefore()),
                Double.doubleToLongBits(checkout.getPriceAfterDiscount()));
    }

    @Test
    public void shouldReturnCheckoutWithDiscount() {
        //given
        ProductRepository mockedRepository = mock(ProductRepository.class);
        when(mockedRepository.findByName("Apple")).thenReturn(new Product(1L, "Apple", 10.0));
        when(mockedRepository.findByName("Banana")).thenReturn(new Product(1L, "Banana", 10.0));
        shoppingService.setProductRepository(mockedRepository);
        shoppingService.setDiscountService(getMockedDiscountService());

        shoppingService.addProductWithQuantity("Apple", 1);
        shoppingService.addProductWithQuantity("Banana", 1);

        //when
        Checkout checkout = shoppingService.checkout();

        //then
        assertEquals(0, shoppingService.getBasket().getProducts().size());
        assertEquals(2, checkout.getProducts().size());
        assertTrue(checkout.getPriceBefore() > checkout.getPriceAfterDiscount());
        assertEquals(Double.doubleToLongBits(13.0), Double.doubleToLongBits(checkout.getPriceAfterDiscount()));

    }

    private DiscountService getMockedDiscountService() {

        DiscountService discountService = new DiscountService();
        DiscountRepository mockedDiscountRepository = Mockito.mock(DiscountRepository.class);
        ArrayList<Discount> availableDiscounts = new ArrayList<>();

        FixedDiscount fixedDiscount = new FixedDiscount();
        fixedDiscount.setPriceAfterDiscount(5);
        fixedDiscount.setRequiredProducts(getRequiredProductsForFixedDiscount());

        PercentageDiscount percentageDiscount = new PercentageDiscount();
        percentageDiscount.setPercentageAmount(20);
        percentageDiscount.setRequiredProducts(getRequiredProductsForPercentageDiscount());

        availableDiscounts.add(fixedDiscount);
        availableDiscounts.add(percentageDiscount);

        when(mockedDiscountRepository.findAll()).thenReturn(availableDiscounts);
        discountService.setRepository(mockedDiscountRepository);
        return discountService;
    }

    private List<DiscountProductWithQuantity> getRequiredProductsForFixedDiscount() {
        ArrayList<DiscountProductWithQuantity> productsForFixedDiscount = new ArrayList<>();
        DiscountProductWithQuantity productWithQuantity = new DiscountProductWithQuantity();
        productWithQuantity.setProduct(new Product(1L, "Apple", 10.0));
        productWithQuantity.setQuantity(1);
        productsForFixedDiscount.add(productWithQuantity);
        return productsForFixedDiscount;
    }

    private List<DiscountProductWithQuantity> getRequiredProductsForPercentageDiscount() {
        ArrayList<DiscountProductWithQuantity> productsForPercentageDiscount = new ArrayList<>();
        DiscountProductWithQuantity productWithQuantity = new DiscountProductWithQuantity();
        productWithQuantity.setProduct(new Product(1L, "Banana", 10.0));
        productWithQuantity.setQuantity(1);
        productsForPercentageDiscount.add(productWithQuantity);
        return productsForPercentageDiscount;
    }
}