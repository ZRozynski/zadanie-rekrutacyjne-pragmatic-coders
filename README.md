# zadanie-rekrutacyjne-pragmatic-coders
Zadanie rekrutacyjne wykonywane w ramach rekrutacji dla firmy Pragmatic Coders.

# O projekcie.
Aplikacja została napisana z wykorzystaniem ``Spring Boot``. 
Decyzja o wykorzystywanej bazie danych to H2.

Pliki SQL, na podstawie których aplikacja ładuje dane do tabel znajdują się w: ``src/main/resources``, 
są to ``products.sql`` oraz ``discounts.sql``.

# O tym co zawarłem, a czego niestety nie udało mi się.
Zadanie wykonałem w całości, starając się dostosować do wszystkich wymagań biznesowych.
Jedynie ze względu na ograniczenia czasowe zdecydowałem się pominąć wątek testów akceptacyjnych, 
ponieważ narzędzie takie jak np ``Test Rest Template`` są dla mnie czymś nowym i potrzebowałbym trochę czasu na zrozumienie zagadnienia.

# Budowanie oraz uruchomienie aplikacji.
Aby zbudować aplikację wystarczy w oknie konsoli (będąc w głównym katalogu z projektem) uruchomić komendę ``mvn clean package``.
Kolejnym krokiem jest uruchomienie pliku ``.jar`` z katalogu ``target/`` poleceniem ``java - jar target/<nazwa pliku .jar>`` 

# API
```
GET: /shop/all - zwraca wszystkie dostępne w danej chwili produkty
```
```
GET: /shop/basket - zwraca obecną zawartość koszyka
```
```
POST: /shop/add?productName=""&quantity="" - zawiera parametry takie jak productName oraz quantity, 
                                             umieszcza pożądany produkt o zadanej ilości w koszyku
```
```
GET: /shop/discounts -  zwraca dostępne promocje
```
```
GET: /shop/checkout - podsumowuje zakupy, zwraca zawartość koszyka wraz z ceną regularną oraz po promocji
```
# Testy
Uruchomienie testów jednostkowych odbywa się poprzez wywołanie polecenia ``mvn test``.
Uruchomienie testów integracyjnych odbywa się poprzez wywyłanie polecenia ``mvn integration-test``.
Bodowa, uruchomienie testów jednostkowych oraz integracyjnych odbywa się poprzez wywołanie polecenia ``mvn verify``.
Domyślnie, budowa oraz testy jednostkowe poprzez komendę ``mvn package``.



